const validate_rules = require('../utils/validate_rules');
const lookup = require('../utils/lookup');
const async = require('async');
const _ = require('lodash');
const moment = require('moment');
const {notification_processes} = require('./notification_processes');

const merge = actions => loadActions(_.chain(actions)
  .castArray()
  .map(({ data }) => data)
  .reduce((acc, datum) => {
    _.forEach(['do', 'dont'], (key) => {
      acc[key] = _.union(datum[key], acc[key]);
    });
    return acc;
  }, {})
  .value());

const isIntersecting = (coll1, coll2) =>
  !_.chain(coll1)
    .intersection(coll2)
    .isEmpty()
    .value();
    
const getActionsBackward = ({ actions, allActions }) => {
  console.log('got actions for backward ',actions);
  _.chain(actions)
    .map(name => _.concat([name], allActions[name]))
    .flatten()
    .filter(dependency => dependency && dependency.constructor === String)
    .uniq()
    .value();
}

const getActionsForward = ({ actions, allActions }) =>
  _.chain(allActions)
    .map((dependencies, action) => (isIntersecting(actions, dependencies) ? action : null))
    .filter(_.negate(_.isNull))
    .concat(actions)
    .uniq()
    .value();


const getAllActions = ({ actions, allActions, getActions }, callback) => {
 // console.log('getAllActions ',actions,allActions,getActions);
  const previousActions = new Set(actions);
  async.doWhilst(
    callback => callback(null, getActions({
      actions: Array.from(previousActions),
      allActions
    })),
    (actions) => {
      if (actions && actions.length === previousActions.size) {
        return false;
      }
      _.forEach(actions, action => previousActions.add(action));
      return true;
    },
    (err, actions) => callback(null, _.pick(allActions, actions))
  );
};
const getAllActionsBackward = (args, callback) => getAllActions(_.extend({}, args, {
  getActions: getActionsBackward
}), callback);

const getAllActionsForward = (args, callback) => getAllActions(_.extend({}, args, {
  getActions: getActionsForward
}), (err, dontActions) => {
  const dontKeys = _.keys(dontActions);
  const allActions = args.allActions;
  return callback(err, _.omit(allActions, dontKeys));
});


// DFS to get all depdenent processes
const getAllDependencies = (actions, process) =>
  _.chain(actions[process])
    .filter(_.isString)
    .map(process => _.concat(getAllDependencies(actions, process), process))
    .flatten()
    .value();

// Expands depdencies of process tree
const expandDependencies = actions =>
  _.forEach(actions, (dependencies, process) => {
    actions[process] = _.union(
      getAllDependencies(actions, process),
      _.castArray(dependencies)
    );
  });

const alignActionsInOrder = (actions, doActions) =>
  _.reduce(doActions, (action1, action2) => {
    if (_.includes(actions[action1], action2)) {
      return action1;
    }
    if (_.has(actions, action2)) {
      actions[action2].unshift(action1);
      return action2;
    }
    return action1;
  });

const getActions = ({ params }, callback) => {
  //console.log('gotta param ',params);
  async.auto({
    job: [(callback) => {
      validate_rules.checkRules({ params }, callback);
    }],
  }, (err, result) =>
    callback(
      err,
      _.extend.apply(
        {},
        _.concat(
          {},
          _.map(
            result,
            value => value
          )
        )
      )
    ));
};

exports.loadActions = ({ params }, callback) => {
  const state = {};
  async.auto({
    load: (callback) => async.auto({
      allActions: callback => getActions({ params }, callback),
    }, (err, { allActions }) => {
      state.actions = allActions;
      return callback(null,allActions);
    }),
    run: ['load',(callback) => {
      if (_.isUndefined(state.actions)) {
        return callback('Actions haven\'t been loaded yet');
      }
      return async.auto(
        state.actions,
        (err, results) => {
          callback();
      });
    }]
	},function(error,results) {
    	callback(error,_.keys(state.actions));
  });
};

exports.runJob = {
  fill_rate: ({params},callback) => {
    let records = [];
    let iterator = (record,next) => {
      let total_pickup = 0,total_order = 0;
      let doc_chunks = _.chunk(_.get(record,'doc'),moment().subtract(1, 'months').daysInMonth());
      _.map(doc_chunks[0],(doc) => {
          total_pickup += doc.total_pickup;
          total_order += doc.total;
      });
      if(total_order < 2 && doc_chunks.length > 1) {
        _.map(doc_chunks[1],(doc) => {
          if(total_order < 2) {
            total_pickup += doc.total_pickup;
            total_order += doc.total;
          }
        });
      }
      record.total = total_order;
      record.total_pickup = total_pickup;
      record.threshold = record.total_pickup/record.total;
      record.last_sent_ts = new Date();
      record.user.platforms = _.map(record.user.platforms,(platform) => _.pick(platform,['name','app_version']));
      records.push(record);
      if(record.total < 2) return next();
      return this.loadActions({params : {job : params.job,data : record,users : params.users,badges : params.badges}},next);
    }
    async.eachSeries(params.data,iterator,(error) => {
      if(error) {
        console.log('ERROR in fill rate action ',error);
      }
      console.log('Actions are taken ');
      callback(error,records);
    });
  },
  pickup_median_rate: ({params},callback) => {
    let records = [];
    let iterator = (record,next) => {
      let pickup_time = [],total_order = 0,count = 0;
      let doc_chunks = _.chunk(_.get(record,'doc'),moment().subtract(1, 'months').daysInMonth());
      _.map(doc_chunks[0],(doc) => {
          pickup_time = _.concat(pickup_time,doc.pickup_time);
          total_order += doc.total;
          count+= doc.pickup_time.length;
      });
      if(total_order < 2 && doc_chunks.length > 1) {
        _.map(doc_chunks[1],(doc) => {
          if(total_order < 2) {
            pickup_time = _.concat(pickup_time,doc.pickup_time);
            total_order += doc.total;
            count++;
          }
        });
      }
      pickup_time = _.sortBy(pickup_time);
      record.total = total_order;
      record.pickup_time = pickup_time;
      record.threshold = _.toSafeInteger(pickup_time[Math.floor(count/2)]);
      record.last_sent_ts = new Date();
      record.user.platforms = _.map(record.user.platforms,(platform) => _.pick(platform,['name','app_version']));
      records.push(record);
      if(record.total < 2) return next();
      return this.loadActions({params : {job : params.job,data : record,users : params.users,badges : params.badges}},next);
    }
    async.eachSeries(params.data,iterator,(error) => {
      if(error) {
        console.log('ERROR in pickup rate action ',error);
      }
      console.log('Actions are taken ');
      callback(error,records);
    });
  },
  feedback_quality: ({params},callback) => {
    let records = [];
    let iterator = (record,next) => {
      let positive = 0,total_order = 0,negative = 0;
      let doc_chunks = _.chunk(_.get(record,'doc'),moment().subtract(1, 'months').daysInMonth());
      _.map(doc_chunks[0],(doc) => {
          positive += doc.positive;
          negative += doc.negative;
          total_order += doc.total;
      });
      if(total_order < 2 && doc_chunks.length > 1) {
        _.map(doc_chunks[1],(doc) => {
          if(total_order < 2) {
            positive += doc.positive;
            negative += doc.negative;
            total_order += doc.total;
          }
        });
      }
      record.total = total_order;
      record.positive = positive;
      record.negative = negative;
      record.threshold = record.positive/record.total;
      record.last_sent_ts = new Date();
      record.user.platforms = _.map(record.user.platforms,(platform) => _.pick(platform,['name','app_version']));
      records.push(record);
      if(record.total < 2) return next();
      return this.loadActions({params : {job : params.job,data : record,users : params.users,badges : params.badges}},next);
    }
    async.eachSeries(params.data,iterator,(error) => {
      if(error) {
        console.log('ERROR in feedback quality rate action ',error);
      }
      console.log('Actions are taken ');
      callback(error,records);
    });
  },
  feedback_price: ({params},callback) => {
    let records = [];
    let iterator = (record,next) => {
      let positive = 0,total_order = 0,negative = 0;
      let doc_chunks = _.chunk(_.get(record,'doc'),moment().subtract(1, 'months').daysInMonth());
      _.map(doc_chunks[0],(doc) => {
          positive += doc.positive;
          negative += doc.negative;
          total_order += doc.total;
      });
      if(total_order < 2 && doc_chunks.length > 1) {
        _.map(doc_chunks[1],(doc) => {
          if(total_order < 2) {
            positive += doc.positive;
            negative += doc.negative;
            total_order += doc.total;
          }
        });
      }
      record.total = total_order;
      record.positive = positive;
      record.negative = negative;
      record.threshold = record.positive/record.total;
      record.last_sent_ts = new Date();
      record.user.platforms = _.map(record.user.platforms,(platform) => _.pick(platform,['name','app_version']));
      records.push(record);
      if(record.total < 2) return next();
      return this.loadActions({params : {job : params.job,data : record,users : params.users,badges : params.badges}},next);
    }
    async.eachSeries(params.data,iterator,(error) => {
      if(error) {
        console.log('ERROR in feedback price rate action ',error);
      }
      console.log('Actions are taken ');
      callback(error,records);
    });
  }
}