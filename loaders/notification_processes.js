const _ = require('lodash');
const request = require('request');
const baseUrl = "http://api.elanic.co";
const authorization = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiNTk0Yjg1OGQ4MDFhY2ExZjg2OTUzNjViIiwiZGV2aWNlX2lkIjoiNDMwOTAyMTQ0NjllNGVlOCJ9.-1s1sWNRKt2CM5HpJUj04B3tYzuqE-DKyBS4LTqDUbI";
const mongoose = require('mongoose');

const notification_processes = ({ params }) => ({
	banUser : callback => {
		let httpOptions = {
			url: `${baseUrl}/bans/${params.data._id}`,
			form: {type : 'profile'},
			headers: {"Authorization": authorization}
		};
		console.log('banUser ',httpOptions.url);
		request.post(httpOptions, (error, resp, body) => callback(error,{}));
	},
	makeUserUnavailable : callback => {
		let httpOptions = {
			url: `${baseUrl}/profiles/${params.data._id}`,
			form: {is_available : false},
			headers: {"Authorization": authorization}
		};
		console.log('makeUserUnavailable ',httpOptions.url);
		request.put(httpOptions, (error, resp, body) => callback(error,{}));
	},

	unapprovedPosts : callback => {
		let httpOptions = {
			url: `${baseUrl}/bulk/posts`,
			form: {data : [{author : `${params.data._id}`,is_available : false}]},
			headers: {"Authorization": authorization}
		};
		console.log('unapprovedPosts ',httpOptions.form);
		request.put(httpOptions, (error, resp, body) => callback(error,{}));
	},
	sendNotification : (callback,result) => {
		let version_validator = _.find(_.get(params.data.user,'platforms',[]),(platform) => {
			let app_version = parseVersionNumber({platform});
			return _.isEqual(platform.name,'android') && _.gte(app_version,3232);
		});
		if(_.isEmpty(version_validator) && params.job.name !== 'fail_rate') return callback();
		let httpOptions = {
			url: `${baseUrl}/notifications`,
			form: params.notificaiton_object,
			headers: {"Authorization": authorization}
		};
		console.log('sent notification ',JSON.stringify(httpOptions.form));
		request.post(httpOptions, (error, resp, body) => callback(error,{}));
	},
	addBadge : callback => {
		let index = _.findIndex(params.users,(user) => _.isEqual(user._id.toString(),`${params.data._id}`));
		let user = (index > -1) ? params.users[index] : {};
		let user_badges = _.get(user,'badges',[]);

		let badges = _.map(params.meta_data.badge,(badge) => mongoose.Types.ObjectId(badge));
		let diff_badges = _.differenceBy(params.badges,badges,_.toString);
		if(_.isEmpty(_.differenceBy(badges,user_badges,_.toString))) return callback();
		let operator = _.isEmpty(user) ? 'add' : 'update';
		let data = {};
		data[`${operator}`] = {"badge": _.get(badges,'0').toString(),"users": [`${params.data._id}`]};
		if(operator === 'update') _.extend(data[`${operator}`],{old_badge : _.get(diff_badges,'0').toString()});
		let httpOptions = {
			url: `${baseUrl}/profiles/all/badges`,
			form: data,
			headers: {"Authorization": authorization}
		};
		console.log('addBadge ',httpOptions.form);
		request.put(httpOptions, (error, resp, body) => callback(error,{}));
	},
	removeBadge : callback => {
		let index = _.findIndex(params.users,(user) => _.isEqual(user._id.toString(),`${params.data._id}`));
		let user = (index > -1) ? params.users[index] : {};
		if(_.isEmpty(user)) return callback();
		let httpOptions = {
			url: `${baseUrl}/profiles/all/badges`,
			form: {"remove": {"badge": params.meta_data.badge,"users": [`${params.data._id}`]}},
			headers: {"Authorization": authorization}
		};
		console.log('removeBadge ',httpOptions.form);
		request.put(httpOptions, (error, resp, body) => callback(error,{}));
	}
})

parseVersionNumber = ({ platform }) => {
  if (!platform.app_version) return 0;
  if (platform.name === 'android') return _.toNumber(platform.app_version.replace(/[\.()]/g, '')) || 0;
  return 0;
};

module.exports = { notification_processes };