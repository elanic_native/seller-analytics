exports.rules = {
  fail_rate : [
    {
      rules : { "and" : [
        {">=" : [ { "var" : "threshold" }, 1 ]},
        {"or": [
          {"!==" : [ { "var" : "last_action" }, "banUser" ]},
          {"and" : [
            {"===" : [ { "var" : "last_action" }, "banUser" ]},
            {">" : [ { "var" : "last_action_days" }, 15 ]}
            ]
          }
        ]}
      ] },
      consequence : {
        "data":{
          "do":["banUser","makeUserUnavailable","sendNotification"],
          "dont": [],
          "meta_data" : {
            "templateName": "SellerFailure_banned_Level_5_New",
            "subject": "ATTN: Your account has 100% failure. Hence your account has been banned.",
            "global_merge_vars":  ["name","tos","toc","tof","fp"],
            "inapp_title": "Your profile has been deactivated.",
            "inapp_subtitle": "Due to 100% failure rate, your profile has been deactivated.",
            "inapp_right_image" : "alert_warning.png",
            "inapp_template_collection": "sellerfailure_inbox_tips_5_all",
            "inapp_template_collection_title": "Level_5"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {">=" : [ { "var" : "threshold" }, 0.75 ]},
        {"<" : [ { "var" : "threshold" }, 1 ]},
        {"or": [
          {"!==" : [ { "var" : "last_action" }, "unapprovedPosts" ]},
          {"and" : [
            {"===" : [ { "var" : "last_action" }, "unapprovedPosts" ]},
            {">" : [ { "var" : "last_action_days" }, 15 ]}
            ]
          }
        ]}
      ] },
      consequence : {
        "data":{
          "do":["unapprovedPosts","sendNotification"],
          "dont": [],
          "meta_data" : {
            "templateName": "SellerFailure_critical_Level_4_New",
            "subject": "ATTN: Your account has a high Seller Failure rate. Your posts has been made unavailable.",
            "global_merge_vars":  ["name","tos","toc","tof","fp"],
            "inapp_title": "Posts marked unavailable.",
            "inapp_subtitle": "Your profile has a failure rate of above 75%.",
            "inapp_right_image" : "alert_warning.png",
            "inapp_template_collection": "sellerfailure_inbox_tips_4_all",
            "inapp_template_collection_title": "Level_4"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {">=" : [ { "var" : "threshold" }, 0.5 ]},
        {"<" : [ { "var" : "threshold" }, 0.75 ]},
        {"or": [
          {"!==" : [ { "var" : "last_action" }, "makeUserUnavailable" ]},
          {"and" : [
            {"===" : [ { "var" : "last_action" }, "makeUserUnavailable" ]},
            {">" : [ { "var" : "last_action_days" }, 15 ]}
            ]
          }
        ]}
      ] },
      consequence : {
        "data":{
          "do":["makeUserUnavailable","sendNotification"],
          "dont": [],
          "meta_data" : {
            "templateName": "SellerFailure_verypoor_Level_3_New",
            "subject": "ATTN: Your account has a high Seller Failure rate. Your closet has been disabled.",
            "global_merge_vars":  ["name","tos","toc","tof","fp"],
            "inapp_title": "Profile has been disabled.",
            "inapp_subtitle": "Your profile has a failure rate of above 50%.",
            "inapp_right_image": "warning.png",
            "inapp_template_collection": "sellerfailure_inbox_tips_3_all",
            "inapp_template_collection_title": "Level_3"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {">=" : [ { "var" : "threshold" }, 0.25 ]},
        {"<" : [ { "var" : "threshold" }, 0.5 ]},
        {"or": [
          {"!==" : [ { "var" : "last_action" }, "makeUserUnavailable" ]},
          {"and" : [
            {"===" : [ { "var" : "last_action" }, "makeUserUnavailable" ]},
            {">" : [ { "var" : "last_action_days" }, 15 ]}
            ]
          }
        ]}
      ] },
      consequence : {
        "data":{
          "do":["makeUserUnavailable","sendNotification"],
          "dont": [],
          "meta_data" : {
            "templateName": "SellerFailure_poor_Level_2_New",
            "subject": "ATTN: Your account has a high Seller Failure rate. Your closet has been disabled.",
            "global_merge_vars":  ["name","tos","toc","tof","fp"],
            "inapp_title": "Profile has been disabled.",
            "inapp_subtitle": "Your profile has a failure rate of above 25%.",
            "inapp_right_image": "warning.png",
            "inapp_template_collection": "sellerfailure_inbox_tips_2all",
            "inapp_template_collection_title": "Level_2"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {">=" : [ { "var" : "threshold" }, 0.11 ]},
        {"<" : [ { "var" : "threshold" }, 0.25 ]},
        {"or": [
          {"!==" : [ { "var" : "last_action" }, "sendNotification" ]},
          {"and" : [
            {"===" : [ { "var" : "last_action" }, "sendNotification" ]},
            {">" : [ { "var" : "last_action_days" }, 15 ]}
            ]
          }
        ]}
      ] },
      consequence : {
        "data":{
          "do":["sendNotification"],
          "dont": [],
          "meta_data" : {
            "templateName": "SellerFailure_belowavg_Level_1_New",
            "subject": "ATTN: Your account has a high Seller Failure rate.",
            "global_merge_vars":  ["name","tos","toc","tof","fp"],
            "inapp_title": "Failure rate reaching 25%.",
            "inapp_subtitle": "Exceeding 25%, your profile will be disabled.",
            "inapp_right_image": "warning.png",
            "inapp_template_collection": "sellerfailure_inbox_tips_1_all",
            "inapp_template_collection_title": "Level_1"
          }
        }, "_type": "actions"
      }
    }
  ],
  fill_rate : [
    {
      rules : { "and" : [
        {">=" : [ { "var" : "threshold" }, 0.98 ]},
        {">=" : [ { "var" : "total" }, 2 ]},
        {"!==" : [ { "var" : "existing_badge" }, "5af56280a0ad51490a8a515a" ]}
      ] },
      consequence : {
        "data":{
          "do":["addBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af56280a0ad51490a8a515a"],
            "inapp_right_image": "banner_0cc3fc0c039b7bc10ee7cefbf6e431c9_fill_rate_gold.png",
            "seller_performance": "5%",
            "badge_name": "stock availability"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {"<" : [ { "var" : "threshold" }, 0.98 ]},
        {">=" : [ { "var" : "threshold" }, 0.9 ]},
        {">=" : [ { "var" : "total" }, 2 ]},
        {"!==" : [ { "var" : "existing_badge" }, "5af562a4a0ad51490a8a515d" ]}
      ] },
      consequence : {
        "data":{
          "do":["addBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af562a4a0ad51490a8a515d"],
            "inapp_right_image": "banner_ac45f3d3afc8a7b18f5b57ad139adae8_fillrate_silver.png",
            "seller_performance": "10%",
            "badge_name": "stock availability"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {"<" : [ { "var" : "threshold" }, 0.9 ]},
        {"===" : [ { "var" : "existing_badge" }, "5af56280a0ad51490a8a515a" ]}
      ] },
      consequence : {
        "data":{
          "do":["removeBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af56280a0ad51490a8a515a"],
            "inapp_right_image": "banner_0cc3fc0c039b7bc10ee7cefbf6e431c9_fill_rate_gold.png",
            "seller_performance": "5%",
            "badge_name": "stock availability"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {"<" : [ { "var" : "threshold" }, 0.9 ]},
        {"===" : [ { "var" : "existing_badge" }, "5af562a4a0ad51490a8a515d" ]}
      ] },
      consequence : {
        "data":{
          "do":["removeBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af562a4a0ad51490a8a515d"],
            "inapp_right_image": "banner_ac45f3d3afc8a7b18f5b57ad139adae8_fillrate_silver.png",
            "seller_performance": "10%",
            "badge_name": "stock availability"
          }
        }, "_type": "actions"
      }
    }
  ],
  pickup_median_rate : [
    {
      rules : { "and" : [
        {"<=" : [ { "var" : "threshold" }, 2 ]},
        {">=" : [ { "var" : "total" }, 2 ]},
        {"!==" : [ { "var" : "existing_badge" }, "5af561b3a0ad51490a8a513a" ]}
      ] },
      consequence : {
        "data":{
          "do":["addBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af561b3a0ad51490a8a513a"],
            "inapp_right_image": "banner_1bb3bf779a56106937ad283b1b6aa11f_pickup_gold.png",
            "seller_performance": "5%",
            "badge_name": "shipping time"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {">" : [ { "var" : "threshold" }, 2 ]},
        {"<=" : [ { "var" : "threshold" }, 3 ]},
        {">=" : [ { "var" : "total" }, 2 ]},
        {"!==" : [ { "var" : "existing_badge" }, "5af561f4a0ad51490a8a5147" ]}
      ] },
      consequence : {
        "data":{
          "do":["addBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af561f4a0ad51490a8a5147"],
            "inapp_right_image": "banner_7e823a6e7177ddf49f9234049e228f54_pickup_silver.png",
            "seller_performance": "10%",
            "badge_name": "shipping time"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {">" : [ { "var" : "threshold" }, 3 ]},
        {"===" : [ { "var" : "existing_badge" }, "5af561b3a0ad51490a8a513a" ]}
      ] },
      consequence : {
        "data":{
          "do":["removeBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af561b3a0ad51490a8a513a"],
            "inapp_right_image": "banner_1bb3bf779a56106937ad283b1b6aa11f_pickup_gold.png",
            "seller_performance": "5%",
            "badge_name": "shipping time"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {">" : [ { "var" : "threshold" }, 3 ]},
         {"===" : [ { "var" : "existing_badge" }, "5af561f4a0ad51490a8a5147" ]}
      ] },
      consequence : {
        "data":{
          "do":["removeBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af561f4a0ad51490a8a5147"],
            "inapp_right_image": "banner_7e823a6e7177ddf49f9234049e228f54_pickup_silver.png",
            "seller_performance": "10%",
            "badge_name": "shipping time"
          }
        }, "_type": "actions"
      }
    }
  ],
  feedback_price : [
    {
      rules : { "and" : [
        {">=" : [ { "var" : "threshold" }, 0.95 ]},
        {">=" : [ { "var" : "total" }, 10 ]},
        {"!==" : [ { "var" : "existing_badge" }, "5af564b462b26148e0c17dd3" ]}
      ] },
      consequence : {
        "data":{
          "do":["addBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af564b462b26148e0c17dd3"],
            "inapp_right_image": "banner_78181e340d5ca1f85d77d19fe41ab8b3_vfm_gold.png",
            "seller_performance": "5%",
            "badge_name": "value for money"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {"<" : [ { "var" : "threshold" }, 0.95 ]},
        {">=" : [ { "var" : "threshold" }, 0.9 ]},
        {">=" : [ { "var" : "total" }, 5 ]},
        {"!==" : [ { "var" : "existing_badge" }, "5af564da62b26148e0c17dd4" ]}
      ] },
      consequence : {
        "data":{
          "do":["addBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af564da62b26148e0c17dd4"],
            "inapp_right_image": "banner_413fdafc8b633164f34483a7e5521b9a_vfm_silver.png",
            "seller_performance": "10%",
            "badge_name": "value for money"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {"<" : [ { "var" : "threshold" }, 0.9 ]},
        {"===" : [ { "var" : "existing_badge" }, "5af564b462b26148e0c17dd3" ]}
      ] },
      consequence : {
        "data":{
          "do":["removeBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af564b462b26148e0c17dd3"],
            "inapp_right_image": "banner_78181e340d5ca1f85d77d19fe41ab8b3_vfm_gold.png",
            "seller_performance": "5%",
            "badge_name": "value for money"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {"<" : [ { "var" : "threshold" }, 0.9 ]},
        {"===" : [ { "var" : "existing_badge" }, "5af564da62b26148e0c17dd4" ]}
      ] },
      consequence : {
        "data":{
          "do":["removeBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af564da62b26148e0c17dd4"],
            "inapp_right_image": "banner_413fdafc8b633164f34483a7e5521b9a_vfm_silver.png",
            "seller_performance": "10%",
            "badge_name": "value for money"
          }
        }, "_type": "actions"
      }
    }
  ],
  feedback_quality : [
    {
      rules : { "and" : [
        {">=" : [ { "var" : "threshold" }, 0.95 ]},
        {">=" : [ { "var" : "total" }, 10 ]},
        {"!==" : [ { "var" : "existing_badge" }, "5af56304a0ad51490a8a515f" ]}
      ] },
      consequence : {
        "data":{
          "do":["addBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af56304a0ad51490a8a515f"],
            "inapp_right_image": "banner_574caed8d096127b2aeb6048d33f24f4_quality_gold.png",
            "seller_performance": "5%",
            "badge_name": "product quality"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {"<" : [ { "var" : "threshold" }, 0.95 ]},
        {">=" : [ { "var" : "threshold" }, 0.90 ]},
        {">=" : [ { "var" : "total" }, 5 ]},
        {"!==" : [ { "var" : "existing_badge" }, "5af5632aa0ad51490a8a5162" ]}
      ] },
      consequence : {
        "data":{
          "do":["addBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af5632aa0ad51490a8a5162"],
            "inapp_right_image": "banner_04616c5f441e00218dd5fb632fc346c4_quality_silver.png",
            "seller_performance": "10%",
            "badge_name": "product quality"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {"<" : [ { "var" : "threshold" }, 0.9 ]},
        {"===" : [ { "var" : "existing_badge" }, "5af56304a0ad51490a8a515f" ]}
      ] },
      consequence : {
        "data":{
          "do":["removeBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af56304a0ad51490a8a515f"],
            "inapp_right_image": "banner_574caed8d096127b2aeb6048d33f24f4_quality_gold.png",
            "seller_performance": "5%",
            "badge_name": "product quality"
          }
        }, "_type": "actions"
      }
    },
    {
      rules : { "and" : [
        {"<" : [ { "var" : "threshold" }, 0.9 ]},
        {"===" : [ { "var" : "existing_badge" }, "5af5632aa0ad51490a8a5162" ]}
      ] },
      consequence : {
        "data":{
          "do":["removeBadge","sendNotification"],
          "dont": [],
          "meta_data" : {
            "badge" : ["5af5632aa0ad51490a8a5162"],
            "inapp_right_image": "banner_04616c5f441e00218dd5fb632fc346c4_quality_silver.png",
            "seller_performance": "10%",
            "badge_name": "product quality"
          }
        }, "_type": "actions"
      }
    }
  ]
}