const mongoose = require('mongoose');
const _ = require('lodash');

const JobRunner = require('../libraries/JobRunner');

class MockRunner extends JobRunner {
  _startRunner() {
    let _time = 0;
    let _timeout;
    this.jobs = [];
    _timeout = setInterval(() => {
      _time += 1000;
      const toBeRunJobs = _.filter(this.jobs, job => _time % job.frequency === 0);
      this.jobs = _.filter(this.jobs, job => _time % job.frequency !== 0);
      _.forEach(
        toBeRunJobs,
        (job) => this.runJob(job)
      );
      if (_.isEmpty(this.jobs)) {
        clearInterval(_timeout);
      }
    }, 1000);
  }

  addJob({ query, frequency }) {
    const _id = mongoose.Types.ObjectId();
    this.jobs.push({
      _id,
      query,
      frequency
    });
  }
}

module.exports = MockRunner;
