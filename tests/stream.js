const streamify = require('../libraries/transform-stream');

const postTransformStream = streamify((chunk, callback) => {
  const modifiedChunk = _.extend(
    {},
    chunk,
    {
      count: _.get(chunk, 'count', 0) + 1
    }
  );
  return callback(null, modifiedChunk);
});
