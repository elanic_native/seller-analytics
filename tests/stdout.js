const $ = require('highland');
const _ = require('lodash');
const streamify = require('../libraries/transform-stream');

/* module.exports = ({ length, debug }) => {
 *   let _length = 0;
 *   const stream = streamify((chunk, next) => {
 *     _length++;
 *     if (debug && (_.isUndefined(length) || _length > length - 1000)) {
 *       console.log(chunk);
 *     }
 *     return next();
 *   });
 *   stream.on('error', err => {
 *     throw new Error(`Stream error is ${err}`);
 *   });
 *   stream.on('finish', () => {
 *     if (!_.isUndefined(length) && _length !== length) {
 *       throw new Error(`Length doesn't match. Expected length is ${length} and received length is ${_length}`);
 *     }
 *   });
 *   return stream;
 * }*/

module.exports = ({ debug }) => {
  return stream => {
    let _length = 0;
    stream.on('data', chunk => {
      _length ++;
      if (debug) {
        console.log('chunk is', JSON.stringify(chunk, null, 2));
      }
    });
    stream.on('end', () => {
      if (debug) {
        console.log('total length is', _length);
      }
    });
    return stream;
  };
}
