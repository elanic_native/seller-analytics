const _ = require('lodash');
const ES = require('../utils/database_handlers/es');

class ESMock extends ES {
  _write({ body }) {
    console.log(
      'buffer is',
      JSON.stringify(
        _.takeRight(body, 4),
        null,
        2
      )
    );
    return Promise.resolve();
  }

  _end({ index }) {
    console.log(`ended writing index ${index} successfully`);
    return Promise.resolve();
  }
}

module.exports = ESMock;
