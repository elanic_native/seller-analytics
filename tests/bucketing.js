const mockDate = require('mockdate');
const moment = require('moment');
const Cron = require('../libraries/Cron');

const testTime = ({ curr, prev, next }) => {

	const offset = 5.5 * 60 * 60 * 1000;

	const lag_offset = 4 * 60 * 60 * 1000;
	mockDate.set(moment(new Date(curr - offset)));

	// cronInstance.fromString("0 */3 * * *");
	// const schedule = cronInstance.schedule();

	const cron = new Cron("0 */3 * * *");

	const { from, to } = cron.getBuckets();

	const testSame = (expected, value) => {
		const prediction = moment(expected - offset - lag_offset);
		if (!value.isSame(prediction)) {
			throw `previous was supposed to be ${prediction} but is actually ${value}`
		}
	}
	testSame(prev, from);
	testSame(next, to);
}

testTime({
	curr: 0,
	prev: - 3 * 60 * 60 * 1000,
	next: 0
});
console.log('All tests successful');