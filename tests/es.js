const _ = require('lodash');
const async = require('async');
const $ = require('highland');

const Query = require('../libraries/Query');
const Cron = require('../libraries/Cron');
const JobRunner = require('../libraries/JobRunner');
const stdout = require('./stdout');
const MongoProfileViewUpdate = require('../streams/mongo-profile-view-update');
const MongoPostViewUpdate = require('../streams/mongo-post-view-update');

const runner = new JobRunner();

const testQuery = (_query, cron) => runner.runJob({
  query: new Query({ _query }),
  cron: new Cron(cron)
});

// Tests

const getAgg = () => ({
  input: {
    database: 'ES',
    index: 'logstash-profileclicks-*',
    "size": 0,
    "query": {
      "bool": {
        "must_not": {
          "term": {
            "verb.params.profile_id.keyword": "myprofile"
          }
        }
      }
    },
    "aggs": {
      "profile": {
        "terms": {
          "field": "object.data.data._id.keyword",
          "size": 9430895
        },
        "aggs": {
          "device-id": {
            "cardinality": {
              "field": "verb.headers.device-id.keyword"
            }
          }
        }
      }
    },
    path: 'aggregations.profile.buckets'
  },
  output: {
    database: 'MongoNew',
    index: 'testingprofileviews'
  },
  streams: [
    MongoProfileViewUpdate()
  ]
});

// Calculates post view count
const getAgg2 = () => ({
  input: {
    database: 'ES',
    index: 'logstash-clicks-*',
    "size": 0,
    "query": {
      "bool": {
        "filter": {
          "exists": {
            "field": "verb.params.post_id"
          }
        }
      }
    },
    "aggs": {
      "post": {
        "terms": {
          "field": "object.data.author._id.keyword",
          "size": 9430895
        },
        "aggs": {
          "device-id": {
            "cardinality": {
              "field": "verb.headers.device-id.keyword"
            }
          }
        }
      }
    },
    path: 'aggregations.post.buckets'
  },
  /* output: {
   *   database: 'MongoNew',
   *   index: 'testingprofileviews'
   * },*/
  streams: [
    MongoPostViewUpdate(),
    stdout({ debug: true })
  ]
});

//testQuery(getAgg(), "* */3 * * *");
testQuery(getAgg2(), "* */3 * * *");
