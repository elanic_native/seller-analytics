const express = require('express');
const router = express.Router();
const analytics = require('../controllers/analytics');

router.get('/users', analytics.profilesAnalytics);
router.get('/users/:author', analytics.profileAnalytics);
router.get('/posts/:author', analytics.userPostAnalytics);
router.get('/posts', analytics.usersPostAnalytics);
router.post('/user_post_analytics', analytics.user_post_analytics);
router.post('/post_analytics', analytics.post_analytics);
router.post('/user_analytics', analytics.user_analytics);

module.exports = router;
