const express = require('express');
const router = express.Router();

const analytics = require('./analytics');
const badges = require('./badges');
const seller_failure = require('./seller_faliure');
const usercrm = require('../controllers/usercrm');

router.use('/badges', badges);
router.use('/seller-failure', seller_failure);
router.use('/analytics', analytics);
router.get('/usercrm/:author', usercrm.usercrm);


module.exports = router;