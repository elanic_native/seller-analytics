const express = require('express');
const router = express.Router();
const seller_failure = require('../controllers/seller_failure');

router.get('/seller-analytics', seller_failure.seller_analytics);
router.post('/seller-analytics-action', seller_failure.seller_analytics_action);

module.exports = router;