const express = require('express');
const router = express.Router();
const badges = require('../controllers/badges');

router.get('/:author/:job_name', badges.badges);

router.post('/feedback_price', badges.feedback_price);
router.post('/feedback_price_action', badges.feedback_price_action);
router.post('/feedback_quality', badges.feedback_quality);
router.post('/feedback_quality_action', badges.feedback_quality_action);
router.post('/fill_rate', badges.fill_rate);
router.post('/fill_rate_action', badges.fill_rate_action);
router.post('/pickup_median_rate', badges.pickup_median_rate);
router.post('/pickup_median_rate_action', badges.pickup_median_rate_action);

module.exports = router;