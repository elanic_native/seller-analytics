const _ = require('lodash');
const $ = require('highland');
const async = require('async');
const dbFactory = require('../utils/database_factory');

class JobRunner {
  
  runJob({ query, cron }, callback) {
   const time_bucket = cron.getBuckets();

    async.auto({
      query: callback => query.getQuery({ time_bucket }, callback),
      input: ['query', (callback, { query }) => {
        const { input } = query;
        const { database } = input;
        const readDb = dbFactory.getInstance({ database });
        return callback(null, readDb.read(input));
      }],
      streams: ['input', 'query', (callback, { input, query }) => {
        const { streams } = query;

        // #HACK Would prefer not to have JobRunner dependent on Highland
        const newStreams = _.concat(
          $.map(
            doc => _.extend(
              {},
              doc,
              { time_bucket }
            )
          ),
          _.castArray(streams || [])
        );
        return callback(
          null,
          input.pipe($.pipeline(...newStreams))
        );
      }],
      output: ['streams', 'query', (callback, { streams, query }) => {
        const { output } = query;
        if (_.isEmpty(output)) {
          return callback(null, streams);
        }
        const { index, chunkSize, database } = output;
        const writeDb = dbFactory.getInstance({
          database
        });
        return callback(
          null,
          streams.pipe(
            writeDb.write({
              chunkSize,
              index
            })
          )
        );
      }]
    }, callback);
  }
  
}

module.exports = JobRunner;
