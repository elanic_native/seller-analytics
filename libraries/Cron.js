const CronConverter = require('cron-converter');

class Cron {
	constructor(cron) {
		this.cron = cron;
	}
	// 	bucket module to give prev bucket time for some lag
	getBuckets() {
		const cronInstance = new CronConverter();

		cronInstance.fromString(this.cron);
		const schedule = cronInstance.schedule();

		const to = schedule.prev().subtract({'hour': '1'});
		const from = schedule.prev().subtract({'hour': '1'});

		return { from , to };
	}
}

module.exports = Cron;
