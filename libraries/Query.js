const _ = require('lodash');
const async = require('async');
const moment = require('moment');

const DATABASE_TYPES = {
  ES: 'ES',
  MONGO: 'MONGO'
};

const setQueryParams = (query, obj) =>
  _.reduce(
    obj,
    (acc, value, key) =>
      _.update(
        acc,
        `input.query.bool.${key}`,
        items => _.concat(
          items || [],
          _.castArray(value)
        )
      ),
    query
  );

class Query {
  
  constructor({ query, _query }) {
    this.state = {}; // #TODO Fix storing query in state
    if (!_.isEmpty(_query)) {
      _.set(this.state, 'query', _query)
    }
  }

  getQuery({ time_bucket }, callback) {
    const query = _.cloneDeep(
      _.get(this.state, 'query', {})
    );
    const { from, to } = time_bucket;
    const compiledQuery = setQueryParams(
      query,
      {
        filter: {
          range: {
            timestamp: {
              gte: moment(from).toISOString(),
              lt: moment(to).toISOString()
            }
          }
        }
      }
    );
    return callback(null, compiledQuery);
  }
}

Query.DATABASE_TYPES = DATABASE_TYPES

module.exports = Query;
