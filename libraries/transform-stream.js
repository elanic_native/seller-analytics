const $ = require('highland');
const through2 = require('through2');

module.exports = (fn) =>
  through2(
    { objectMode: true, allowHalfOpen: false },
    (chunk, enc, cb) => fn(chunk, cb)
  )
