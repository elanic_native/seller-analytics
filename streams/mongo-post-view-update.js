const $ = require('highland');
const mongoose = require('mongoose')
const _ = require('lodash');
const moment = require('moment');

module.exports = () => {
  return s =>
    $([
      {
        type: 'createIndex',
        keys: {
          profile_id: 1
        },
        options: {
          unique: true
        }
      }, {
        type: 'createIndex',
        keys: {
          'post_views.bucket': 1
        }
      }, {
        type: 'createIndex',
        keys: {
          'post_views.count': 1
        }
      }, {
        type: 'createIndex',
        keys: {
          'post_views_sum': 1
        }
      }
    ]).concat(
      s.map(
        doc => {
          const bucket = moment(_.get(doc, 'time_bucket.from')).toISOString();
          const count = _.get(doc, 'device-id.value');
          const profile_id = mongoose.Types.ObjectId(_.get(doc, 'key'));
          return {
            type: 'updateOne',
            filter: {
              profile_id,
              'post_views.bucket': {
                $ne: bucket
              }
            },
            update: {
              $addToSet: {
                post_views: {
                  bucket,
                  count
                }
              },
              $inc: {
                post_views_sum: count
              }
            },
            setOnInsert: {
              profile_id,
              post_views: [{
                bucket,
                count
              }],
              post_views_sum: count
            },
            upsert: true
          };
        }
      )
    )
}
