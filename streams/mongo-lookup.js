const _ = require('lodash');
const streamify = require('../libraries/transform-stream');

module.exports = () => {
  let _buffer = [];
  return streamify((chunk, next) => {
    /* console.log('chunk is', chunk);*/
    return next(null, chunk);
  });
}
