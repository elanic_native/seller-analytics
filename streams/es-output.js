const $ = require('highland');
const _ = require('lodash');

module.exports = ({ index }) => {
  return $.pipeline(
    $.map(doc => [
                   {
                     update: {
                       _id: _.get(doc, '_id'),
                       _type: "doc",
                       _index: index
                     }
                   }, {
                     doc: _.extend(
                       {},
                       _.omit(doc, ['_id']),
                       { key: _.get(doc, '_id') }
                     ),
                     "doc_as_upsert" : true
                   }
    ]),
    $.flatten()
  );
};
