const _ = require('lodash');
const streamify = require('../libraries/transform-stream');

module.exports = (map) => {
  return streamify((chunk, next) => {
    return next(null, map(chunk));
  });
}
