class Job {
	constructor({name,input_database,output_database,entity,group}) {
		this.name = name;
		this.input_database = input_database;
		this.output_database = output_database;
		this.entity = entity;
		this.group = group;
	}
}

module.exports = Job;