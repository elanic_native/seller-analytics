const _ = require('lodash');
const async = require('async');
const mongoose = require('mongoose');
const $ = require('highland');

const base = require('./base');

const executeOperations = (conn) => (ops) => {
  if (_.isEmpty(ops)) {
    return $(Promise.resolve(null));
  }
  const groupedOps = _.groupBy(ops, 'type');
  const bulkWrite = _
    .chain(groupedOps)
    .omit(['createIndex'])
    .values()
    .flatten()
    .value();
  const indexes = groupedOps['createIndex'];
  const promises = [];
  
  if (!_.isEmpty(indexes)) {
    _.each(
      indexes,
      ({ keys, options }) =>
        promises.push(
          conn.createIndex(keys, options)
        )
    )
  }

  if (!_.isEmpty(bulkWrite)) {
    const bulkOps = _.map(
      bulkWrite,
      obj => ({
        [_.get(obj, 'type')]: _.omit(obj, ['type'])
      })
    );
    promises.push(
      conn.bulkWrite(
        bulkOps,
        { ordered: false }
      )
    )
  }
  return $(Promise.all(promises));
}

class Mongo extends base {
  
  constructor({ url, options }) {
    super();
    this.client = mongoose.createConnection(url, options);
  }

  read({ index }) {
    
  }

  write({ index, chunkSize = 100 }) {
    const conn = this.client.collection(index);
    return $.pipeline(
      $.batchWithTimeOrCount(100, chunkSize),
      $.flatMap(executeOperations(conn)),
      $.flatten,
      $.map(chunk => {
        let resp;
        try {
          resp = chunk.toJSON();
        } catch(e) {
          return;
        }
        return _.get(resp, 'writeErrors');
      }),
      $.flatten(),
      $.filter(err => err && err.code !== 11000),
      stream => {
        stream.on('data', chunk => {
          if (chunk) {
            throw chunk;
          }
        });
        stream.on('error', (err) => {
          throw err;
        });
        stream.on('end', () => {
          this.client.close();
        });
        return stream;
      }
    );
  }
  
}

module.exports = Mongo;
