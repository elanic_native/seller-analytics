const base = require('./base');
const async = require('async');
const mongoose = require('mongoose');
const _ = require('lodash');
const MongoClient = require('mongodb').MongoClient;
const template = require('./template').template;

let database = {
	adapter: 'mongodb',
	host: 'test.elanic.co',
	port: '27017',
	name: 'elanicdb',
	username: 'mongoadmin',
	password: 'elanicAdmin2015'
};

//let uri = `${database.adapter}://${database.username}:${database.password}@${database.host}:${database.port}/${database.name}`;
let uri = "mongodb://mongoadmin:elanicAdmin2015@10.139.104.190:27017,10.139.232.143:27017,10.139.232.151:27017,10.139.232.150:27017,10.139.224.200:27017/elanicdb?replicaSet=elanic_rs&readPreference=secondaryPreferred";

class mongo extends base {
	constructor() {
		super();
	}

	initDB({url}, callback) {
		if(this.db) return callback(null,this.db);
		let self = this;
		MongoClient.connect(uri,(err, db) => {
			self.setDB(self,db);
			callback(err,db);
		});
	}

	setDB(db) {
		this.db = db;
	}

	execQueries({job,db,skip,limit,type,time_duration,subtract_date},callback) {
		let collection = db.collection(job.group);
		let pipeline = template.createPipeline({job,skip,limit,type,time_duration,subtract_date});
  		collection.aggregate(pipeline,{allowDiskUse : true},callback);
	}

	execSimpleQueries({job,db,filter,select,entity},callback) {
		entity = entity || job.entity;
		let collection = db.collection(`${entity}`);
		let query = collection.find(filter);
		query.toArray((error,doc)=> {
			callback(error,doc);
		});
	}

	writeOutput({job,db,data,filter_key},callback) {
		let collection = db.collection(job.entity).initializeUnorderedBulkOp();
		let iterator = (record,next) => {
			record.author = mongoose.Types.ObjectId(record._id);
			record._id = mongoose.Types.ObjectId();
			_.extend(record,{job_name : job.name});
			collection.find({_id : record._id}).upsert().update({$set : record});
			next();
		}
		async.each(data,iterator,function(error){
			collection.execute(callback);
		});
	}

	updateBulk({job,db,data},callback) {
		let collection = db.collection(job.entity).initializeUnorderedBulkOp();
		let iterator = (record,next) => {
			record.author = mongoose.Types.ObjectId(record._id);
			delete record._id;
			_.extend(record,{job_name : job.name});
			collection.find({author : record.author,job_name : record.job_name}).upsert().update({$set : record});
			next();
		}
		async.each(data,iterator,function(error){
			collection.execute(callback);
		});
	}
}

module.exports = mongo;