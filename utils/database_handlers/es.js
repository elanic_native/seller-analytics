const _ = require('lodash');
const async = require('async');
const elasticsearch = require('elasticsearch');
const $ = require('highland');

const esOutputStream = require('../../streams/es-output');
const base = require('./base');

class ES extends base {
  
  constructor({ host, hosts }) {
    super();
    this.client = new elasticsearch.Client({ host, hosts });
  }

  read({ index, filter, aggs, path, size, query }) {
    // #TODO can refactor this to parse the stream as it's coming from ES
    return $((push) => {
      this.client.search({
        index,
        requestCache: true,
        body: {
          size,
          query,
          filter,
          aggs
        }
      }).then(body => {
        _.forEach(
          _.get(
            body,
            path
          ),
          hit => push(null, hit)
        )
        push(null, $.nil);
      }).catch(push);
    });
  }

  _write({ body }) {
    return this.client.bulk({ body });
  }

  _end({ index }) {
    return this.client.indices.putAlias({
      index,
      name: `complete-${index}`
    });
  }

  write({ index, chunkSize = 1000 }) {
    // #TODO should create a writable stream that sends to ES directly
    const stream = esOutputStream({ index })
    let _buffer = [];

    // #HACK hacky code. Not sure how to fix this problem here.
    // Will fix it later when I learn more.
    const _globalErrors = [];
    const _globalPromises = [];
    const flushBuffer = () => {
      if (_.isEmpty(_buffer)){
        return;
      }
      _globalPromises.push(
        this._write({
          body: _buffer
        }).then().catch(err => _globalErrors.push(err))
      );
      _buffer = [];
    };
    stream.on('data', chunk => {
      _buffer.push(chunk);
      if (_buffer.length >= chunkSize) {
        flushBuffer();
      }
    });
    stream.on('end', () => {
      flushBuffer();
      Promise.all(_globalPromises).then(() => {
        const errors = _.filter(
          _globalErrors,
          _.identity
        );
        if (!_.isEmpty(errors)) {
          return console.log(
            'errors while indexing are',
            errors
          );
        }
        this._end({ index }, (err) => {
          if (err) {
            console.log('error while aliasing index is', err);
          }
        });
      });
    });
    return stream;
  }
  
}

module.exports = ES;
