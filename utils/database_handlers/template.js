const _ = require('lodash');
const moment = require('moment');
let template = {};

const job_params = {
	fail_rate : {
		pipeline_tags : [
			'status_filter_seller',
			'ordertrack_lookup',
			'unwind_ordertrack',
			'add_fail_tag_fields',
			'fail_tag_not_allowed',
			'fail_tag_allowed',
			'fail_rate_date',
			'fail_rate_date_check',
			'seller_failed',
			'seller_failed_group',
			'per',
			'add_timestamp',
			'skip',
			'limit'
		],
		output_tags : [
			'job_filter_seller',
			'ts_sort',
			'user_lookup',
			'unwind_user',
			'group_current',
			'add_timestamp',
			'skip',
			'limit'
		],
		tags: ['inspection_failed','pickup_rescheduled','pickup_scheduled','seller_no_response_failed','availability_failed','seller_order_cancelled','pickup_failed'],
		dontAllowedTags: ['payment_confirmation_failed'],
		statuses: ['cancelled','open'],
		status_operator: "$nin",
		operator: 'or',
		field: 'filter_tag.tag',
		unwind_field: 'filter_tag',
		date_range_field: 'filter_tag.created_on',
		threshold : 3,
		seller: ['5b76acf4b491a9698eea0829','59e71c0bd84f1b1ffc5d13b1','5afe67ecf57e372be5fbcbf6','5c3c2a36747e8f15aef09a50']
	},
	fill_rate : {
		pipeline_tags : [
			'status_filter',
			'group_all_records',
			'unwind',
			'ordertrack_lookup',
			'unwind_ordertrack',
			'tag_all_filter',
			'unwind_ordertrack_tag',
			'pickup_tag_date_range',
			'group_id',
			'per',
			'add_timestamp',
			'skip',
			'limit'
		],
		output_tags : [
			'job_filter',
			'ts_sort',
			'user_lookup',
			'unwind_user',
			'platform_lookup',
			'group_fill_current',
			'add_timestamp',
			'skip',
			'limit'
		],
		tags: ['admin_inventory_not_available','pickup_success'],
		statuses: ['open'],
		threshold : 3,
		unwind_field: 'doc',
		local_lookup_track_field: 'doc.track',
		threshold_field: 'total_pickup'
	},
	pickup_median_rate : {
		pipeline_tags : [
			'status_filter',
			'ordertrack_lookup',
			'unwind_ordertrack',
			'unwind_ordertrack_tag',
			'tag_filter',
			'group_track_tag',
			'check_count_greater_than_one',
			'tag_time_difference',
			'time_sort',
			'group_time',
			'calculate_median',
			'add_timestamp',
			'skip',
			'limit'
		],
		output_tags : [
			'job_filter',
			'ts_sort',
			'user_lookup',
			'unwind_user',
			'platform_lookup',
			'group_pickup_median_current',
			'add_timestamp',
			'skip',
			'limit'
		],
		tags: ['admin_inventory_not_available','pickup_success'],
		statuses: ['open'],
		threshold : 3
	},
	return_rate : {
		pipeline_tags : [
			'status_filter',
			'ordertrack_lookup',
			'unwind_ordertrack',
			'add_field_status_check',
			'divide_total',
			'check_lower_threshold',
			'skip',
			'limit'
		],
		statuses: ['completed','returned'],
		check_status : ['returned'],
		threshold : 0.06
	},
	feedback_quality : {
		pipeline_tags : [
			'status_filter',
			'group_seller',
			'feedback_lookup',
			'unwind_feedback',
			'feedback_filter',
			'question_lookup',
			'unwind_question',
			'answer_lookup',
			'unwind_answer',
			'match_answer',
			'question_filter',
			'sum_question',
			'check_greater_than_zero',
			'add_timestamp',
			'skip',
			'limit'
		],
		output_tags : [
			'job_filter',
			'ts_sort',
			'user_lookup',
			'unwind_user',
			'platform_lookup',
			'group_feedback_current',
			'add_timestamp',
			'skip',
			'limit'
		],
		statuses: ['cancelled','failed'],
		answer_display: 'Product Quality',
		positive_question:"What went well?",
		negative_question:"What could be improved ?"
	},
	feedback_price : {
		pipeline_tags : [
			'status_filter',
			'group_seller',
			'feedback_lookup',
			'unwind_feedback',
			'feedback_filter',
			'question_lookup',
			'unwind_question',
			'answer_lookup',
			'unwind_answer',
			'match_answer',
			'question_filter',
			'sum_question',
			'check_greater_than_zero',
			'add_timestamp',
			'skip',
			'limit'
		],
		output_tags : [
			'job_filter',
			'ts_sort',
			'user_lookup',
			'unwind_user',
			'platform_lookup',
			'group_feedback_current',
			'add_timestamp',
			'skip',
			'limit'
		],
		statuses: ['cancelled','failed'],
		answer_display: 'Product Price',
		positive_question:"What went well?",
		negative_question:"What could be improved ?"
	}
}

template.createPipeline = ({job,skip,limit,type,time_duration,subtract_date}) => {
	let pipeline = [];
	let days_span = time_duration || 7;
	let days_range = subtract_date || 0;
	let params = job_params[_.get(job,'name')];
	_.extend(params,{job,startDate : moment().subtract(days_range + days_span, 'days').toDate(),endDate: moment().subtract(days_range,'days').toDate(),skip,limit});
	_.map(params[`${type + '_tags'}`],(tag) => {
		pipeline.push(pipeline_tags[tag](params));
	});
	console.log('JSON ',JSON.stringify(pipeline));
	return pipeline;
}

// const status_filter = (tag,params) => {
// 	let tag_fields = _.split(tag,':');

// }

const sort = (params) => {$sort : {ts : -1}}

const pipeline_tags = {
  tag_filter: params => ({$match:{'ordertrack.tags.tag':{$in: _.get(params,'tags',[])}}}),
  tag_condition_filter: params => ( {$addFields:{total_failed: {$cond: [createConditionExpression({params}),1,0]}}}),
  tag_not_allowed: params => ({$match:{'ordertrack.tags.tag':{$nin : params.dontAllowedTags}}}),
  second_last_tag: params => ({$addFields:{filter_tag:{$slice:[{$slice:['$ordertrack.tags',-2]},1]}}}),
  unwind: params => ( {$unwind:`$${_.get(params,'unwind_field',[])}`}),
  status_filter_seller: params => ({$match : {is_invalid:false,created_on:{$gte: moment().subtract(2, 'months').toDate(),$lt: moment().toDate()},status: {$nin : _.get(params,'statuses',['active'])},seller : {$nin : _.get(params,'seller')}}}),
  status_filter: params => ({$match : {is_invalid:false,created_on:{$gte: moment().subtract(2, 'months').toDate(),$lt: moment().toDate()},status: {$nin : _.get(params,'statuses',['active'])}}}),
  ordertrack_lookup: params => lookup({from:'ordertracks',localField:_.get(params,'local_lookup_track_field','track'),foreignField:'_id'}),
  unwind_ordertrack: params => unwind({field:'ordertrack'}),
  unwind_ordertrack_tag: params => unwind({field: 'ordertrack.tags'}),
  add_field_status_check: params => ({$addFields:{sum_condition:{$cond:[{$eq:['$status',_.get(params,'check_status')]},1,0]}}}),
  divide_total: params => ({$group:{_id:'$seller', total:{$sum:1}, total_failed:{$sum: '$total_failed'},"total_completed" : {"$sum" : "$complete_order"}}}),
  check_lower_threshold: params => ({$match:{threshold:{$gt:0}}}),
  check_count_greater_than_one: params => ({$match:{'count':{$gt:1},"track_last.created_on" : {"$gte": params.startDate,"$lt": params.endDate}}}),
  group_track_tag: params => ({$group:{_id:'$_id', track_first:{$first:'$ordertrack.tags'}, track_last:{$last:'$ordertrack.tags'}, count:{$sum:1}, seller:{$max:'$seller'}}}),
  tag_time_difference: params => ( {$addFields :{time:{$divide:[{$subtract:['$track_last.created_on','$track_first.created_on']},86400000]}}}),
  group_avg: params => ({$group:{_id:'$seller',threshold:{$avg:'$time'}, count:{$sum:1}}}),
  per: params => ({$addFields:{threshold:{$divide:[`$${_.get(params,'threshold_field','total_failed')}`,'$total']}}}),
  add_complete_field: params => ( {"$addFields":{"complete_order": {"$cond":[{$eq : ["$status","completed"]},1,0]}}}),
  add_timestamp: params => ( {"$addFields":{"ts": params.endDate}}),
  user_lookup: params => ({"$lookup":{"from":"userprofiles","localField":"author","foreignField":"_id","as":"user"}}),
  unwind_user: params => unwind({field : 'user'}),
  project_fields: params => ({"$project": {"name" : "$user.name.first" + " " + "$user.name.last","tags" : "$user.tags","phone_number" : "$user.phone.number","email" : "$user.email", "username" : "$user.username","devices" : "$user.devices","ts" : 1,"total_completed" : 1,"threshold" : 1,"total" : 1,"total_failed" : 1}}),
  project_limit: params => ({$project : {'_id' : 1,'ts' : 1,doc : {$slice : ['$doc',5]}}}),
  skip: params => ({$skip : _.get(params,'skip',0)}),
  limit: params => ({$limit : _.get(params,'limit',0)}),
  group_current: params => ({"$group":{"_id":"$author",user : {$first : "$user"},"doc":{"$push":{total : "$$CURRENT.total",total_failed : "$$CURRENT.total_failed",total_completed : "$$CURRENT.total_completed",threshold : "$$CURRENT.threshold",ts :  "$$CURRENT.ts"}}}}),
  group_fill_current: params => ({"$group":{"_id":"$author","user":{"$first":"$user"},"doc":{"$push":{"total":"$$CURRENT.total","total_pickup":"$$CURRENT.total_pickup","threshold":"$$CURRENT.threshold","ts":"$$CURRENT.ts"}}}}),
  group_pickup_median_current: params => ({"$group":{"_id":"$author","user":{"$first":"$user"},"doc":{"$push":{"total":"$$CURRENT.total","pickup_time":"$$CURRENT.pickup_time","ts":"$$CURRENT.ts"}}}}),
  group_feedback_current: params => ({"$group":{"_id":"$author",user : {$first : "$user"},"doc":{"$push":{total : "$$CURRENT.total",positive : "$$CURRENT.count_positive",negative : "$$CURRENT.count_negative",threshold : "$$CURRENT.threshold",ts :  "$$CURRENT.ts"}}}}),
  job_filter: params => ({$match : {job_name : _.get(params,'job.name'),ts : {$gte : moment().subtract(2, 'months').toDate()}}}),
  job_filter_seller: params => ({$match : {job_name : _.get(params,'job.name'),ts : {$gte : moment().subtract(2, 'months').toDate()},author : {$nin : _.get(params,'seller')}}}),
  ts_sort: params => ({$sort : {ts : -1}}),
  group_all_records: params => ({$group : {_id : '$seller',total : {$sum : 1},doc : {$push : '$$CURRENT'}}}),
  tag_all_filter: params => ({$match:{'ordertrack.tags.tag':{$all: _.get(params,'tags',[])}}}),
  group_id: params => ({$group : {_id : '$_id',total_pickup : {$sum : 1},total : {$max : '$total'}}}),
  time_sort: params => ({$sort : {time : 1}}),
  group_time: params => ({$group:{_id:'$seller',doc : {$push : '$$CURRENT.time'}, count:{$sum:1}}}),
  calculate_median: params => ({$project : {total : "$count",pickup_time : "$doc"}}),
  group_seller: params => ({$group:{_id:'$seller', count:{$sum:1}}}),
  feedback_lookup: params => ({$lookup:{from:'feedbacks',localField:'_id',foreignField:'profile',as:'feedback'}}),
  unwind_feedback: params => ({$unwind:{path:'$feedback', preserveNullAndEmptyArrays:true}}),
  feedback_filter: params => ({$match:{$or:[{'feedback.created_on':{$gt: params.startDate,$lte: params.endDate}},{'feedback':{$exists:false}}]}}),
  question_lookup: params => ({$lookup:{from:'questions',localField:'feedback.question',foreignField:'_id',as:'question'}}),
  unwind_question: params => ({$unwind:{path:'$question', preserveNullAndEmptyArrays:true}}),
  answer_lookup: params => ({$lookup:{from:'answers',localField:'feedback.answer._id',foreignField:'_id',as:'answers'}}),
  unwind_answer: params => ({$unwind:{path:'$answers',preserveNullAndEmptyArrays:true}}),
  match_answer: params => ({$match:{$or:[{'answer.display':_.get(params,'answer_display')},{'answer':{$exists:false}}]}}),
  question_filter: params => ({$group:{_id:'$_id', count_positive:{$sum:{$cond:[{$eq:['$question.display',_.get(params,'positive_question')]},1,0]}},
	count_negative:{$sum:{$cond:[{$eq:['$question.display',_.get(params,'negative_question')]},1,0]}}}}),
  sum_question: params => ({$addFields:{total:{$sum:['$count_positive','$count_negative']}}}),
  check_greater_than_zero: params => ({$match : {total : {$gte : 0}}}),
  platform_lookup: params => ({$lookup:{from:'platforms',localField:'user.devices',foreignField:'device_id', as:'user.platforms'}}),
  date_range_filter: params => ({"$match" : {"filter_tag.created_on" :{"$gte": params.startDate,"$lt": params.endDate}}}),
  pickup_tag_date_range: params => ({"$match":{"ordertrack.tags.tag" : "pickup_success","ordertrack.tags.created_on":{"$gte": params.startDate,"$lt": params.endDate}}}),
  add_fail_tag_fields: params => ({"$addFields":{
        "payment_conf_failed":{"$arrayElemAt":[
            {"$filter":{
                "input":"$ordertrack.tags",
                "as":"tt",
                "cond":{"$eq":["$$tt.tag","payment_confirmation_failed"]}
                }},0
            ]},
            "pickup":{"$arrayElemAt":[
            {"$filter":{
                "input":"$ordertrack.tags",
                "as":"tt",
                "cond":{"$or":[{"$eq":["$$tt.tag","pickup_success"]},{"$eq":["$$tt.tag","delivery_intransit"]}, {"$eq":["$$tt.tag","pickup_intransit"]}]}
                }},0
            ]},
            "seller_failed":{"$arrayElemAt":[
            {"$filter":{
                "input":"$ordertrack.tags",
                "as":"tt",
                "cond":{"$or":[{"$eq":["$$tt.tag","seller_order_cancelled"]}, {"$eq":["$$tt.tag","availability_failed"]}, {"$eq":["$$tt.tag","inspection_failed"]}, {"$eq":["$$tt.tag","seller_no_response_failed"]},{"$eq":["$$tt.tag","pickup_failed"]}]}
                }},0]}}}),
  fail_tag_not_allowed: params => ({"$match":{"payment_conf_failed":{"$exists":false}}}),
  fail_tag_allowed: params => ({"$match":{"$or":[{"pickup":{"$exists":true}},{"seller_failed":{"$exists":true}}]}}),
  fail_rate_date: params => ({"$addFields":{"last_tag":{"$max":["$pickup.created_on","$seller_failed.created_on"]}}}),
  fail_rate_date_check: params => ({"$match":{"last_tag":{"$gte": params.startDate,"$lt": params.endDate}}}),
  seller_failed: params => ({"$addFields":{"seller_failed":{"$cond":[{"$eq":["$last_tag","$seller_failed.created_on"]},1,0]}}}),
  seller_failed_group: params => ({"$group":{"_id":"$seller","total":{"$sum":1},"total_failed":{"$sum":"$seller_failed"}}}),
}

// const pipeline_tags2 = {
// 	status_filter: {$match : {is_invalid:false,created_on:{$gt: params.startDate,$lt: params.endDate},status: {$nin : {$_get: ['statuses', ['active']]}}}},
// 	sum_macro: {$group: { count:{$sum: {$_get: ['group_name', 1]}} } },
// 	group_avg: {$_merge: [{ $group:{ _id:'$seller',threshold:{$avg:'$time'} }}, 'sum_macro']},
// };

// const operators = (params) => ({
// 	$_get: (path, value) => _.get(params, path, value),
// 	$_merge: (values) => _.chain(values).map(parser).merge().value(),
// });

// const parser = ({ tags, params }) => {

// }

const createConditionExpression = ({params}) => {
	let condition = {};
	condition[`$${_.get(params,'operator')}`] = _.map(_.get(params,'tags'),(tag) => {return {$eq:[`$${_.get(params,'field')}`,tag]}});
	return condition;
}

const group = ({field,count_field}) => {
	let group = {$group : {_id : `$${field}`}};
	if(count_field) group.$group.count = {$sum : count_field};
	return group;
}

const lookup = ({from,localField,foreignField}) => {return {$lookup : {from,localField,foreignField,as : _.trimEnd(from,'s')}}}

const unwind = ({field,removeNull}) => {
	let unwind = {$unwind : {path : `$${field}`}};
	if(removeNull) unwind.$unwind.preserveNullAndEmptyArrays = true;
	return unwind;
}
exports.template = template;