
const database_factory = require('../database_factory');

let db = '';
// mongoose.connect(config.uriTest, options);

function connectDb(callback) {
    let database = database_factory.getDB({ name: 'mongo' });
    let db = new database();
    db.initDB({}, (error, database_connection) => {
        return callback(null, database_connection);
    });
};
function getCollection(collection, callback) {
    return connectDb((err, database_connection) => 
        callback(err, err || database_connection.collection(collection))
    );
}

module.exports = { connectDb, getCollection };