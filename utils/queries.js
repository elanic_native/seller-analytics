const  MongoProfileViewUpdate  = require('../streams/mongo-profile-view-update');
const  MongoPostViewUpdate  = require('../streams/mongo-post-view-update');
const  MongoPostUpdate  = require('../streams/mongo-post-update');

const queries = {
  profile_view: {
    input: {
      database: 'ESNew',
      index: 'logstash-profileclicks-*',
      "size": 0,
      "query": {
        "bool": {
          "must_not": {
            "term": {
              "verb.params.profile_id.keyword": "myprofile"
            }
          }
        }
      },
      "aggs": {
        "profile": {
          "terms": {
            "field": "object.data._id.keyword",
            "size": 9430895
          },
          "aggs": {
            "device-id": {
              "cardinality": {
                "field": "verb.headers.device-id.keyword"
              }
            }
          }
        }
      },
      path: 'aggregations.profile.buckets'
    },
    output: {
      database: 'MongoProd',
      index: 'user_analytics'
    },
    streams: [
      MongoProfileViewUpdate()
    ],
    frequency : "0 */3 * * *"
  },
  post_view: {
    input: {
      database: 'ESNew',
      index: 'logstash-clicks-*',
      "size": 0,
      "query": {
        "bool": {
          "filter": {
            "exists": {
              "field": "object.data._id"
            }
          }
        }
      },      
      "aggs": {
        "post": {
          "terms": {
            "field": "object.data._id.keyword",
            "size": 2000000
          }
        }
      },
      path: 'aggregations.post.buckets'
    },
    output: {
      database: 'MongoProd',
      index: 'post_analytics'
    },
    streams: [
      MongoPostUpdate()
    ],
    frequency : "0 */3 * * *"
  },
  profile_post_view: {
    input: {
      database: 'ESNew',
      index: 'logstash-clicks-*',
      "size": 0,
      "query": {
        "bool": {
          "filter": {
            "exists": {
              "field": "object.data._id"
            }
          }
        }
      },
      "aggs": {
        "post": {
          "terms": {
            "field": "object.data.author._id.keyword",
            "size": 9430895
          },
          "aggs": {
            "device-id": {
              "cardinality": {
                "field": "verb.headers.device-id.keyword"
              }
            }
          }
        }
      },
      path: 'aggregations.post.buckets'
    },
    output: {
      database: 'MongoProd',
      index: 'user_posts_analytics'
    },
    streams: [
      MongoPostViewUpdate()
    ],
    frequency : "0 */3 * * *"
  }
}

module.exports = queries;