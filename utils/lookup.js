const _ = require('lodash');

const _data = {};

const lookup = {
  set: (key, value) => {
    if (_.has(_data, key)) {
      throw `Reflection collision error! ${key} already defined.`;
    }
    _.set(_data, key, value);
  },
  get: key => _.get(_data, key)
};

lookup.load = (data) => {
  const type = _.get(data, '_type');
  const params = _.get(data, 'data');
  if (_.isEmpty(type)) {
    return null;
  }
  const loader = lookup.get(type);
  if (loader) {
    return loader(params);
  }
  return null;
};

module.exports = lookup;
