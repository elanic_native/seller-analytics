const mongo = require('./database_handlers/mongo');
const MongoNew = require('./database_handlers/mongo-new');
const ES = require('./database_handlers/es');
const ESMock = require('../tests/ESMock.js');

const databases = {
  mongo,
  ES,
  ESMock,
  MongoNew,
  MongoProd: MongoNew,
  ESNew: ES
};

const esUrl = 'http://test.elanic.co:7231';
const mongoUrl = 'mongodb://mongoadmin:elanicAdmin2015@test.elanic.co:27017/elanicdb';
const dbParams = {
  ES: {
    host: esUrl
  },
  ESNew: {
    hosts: [
      '10.139.40.227:9200',
      '10.139.104.238:9200',
      '10.139.240.85:9200'
    ]
  },
  ESMock: {
    url: esUrl
  },
  MongoNew: {
    url: mongoUrl
  },
  MongoProd: {
    url: 'mongodb://mongoadmin:elanicAdmin2015@10.139.104.190:27017,10.139.232.143:27017,10.139.232.151:27017,10.139.232.150:27017,10.139.224.200:27017/elanicdb?replicaSet=elanic_rs&readPreference=secondaryPreferred'
  }
}

exports.getDB = ({name}) => {
  return databases[name];
}

exports.getInstance = ({ database }) => {
  const db = databases[database];
  const params = dbParams[database];
  return new db(params);
}
