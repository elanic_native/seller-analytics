const jsonLogic = require('json-logic-js');
const rules = require('../loaders/action_rules').rules;
const _ = require('lodash');
const {notification_processes} = require('../loaders/notification_processes');
const moment = require('moment');

exports.checkRules = ({params},callback) => {
	let consequences = [];
	_.map(rules[_.get(params,'job.name')],(condition) => { 
		if(validateRules(condition.rules,params.data)) consequences.push(condition.consequence);
	});
	return parseParams({params,consequences},callback);
};

const parseParams = ({params,consequences},callback) => {
	let actions = {};
	let filter_actions = {};
	if(params.job.name === 'fail_rate') {
		_.map(consequences,(consequence) => {
			let data = {};
			data[_.get(params,'data._id','').toString()] = {
				name: !_.isEmpty(_.get(params,'data.user.name.first')) ? _.get(params,'data.user.name.first') + " " + _.get(params,'data.user.name.last') : _.get(params,'data.user.username'),
				tos: _.get(params,'data.total'),
				toc: _.get(params,'data.total_completed'),
				tof: _.get(params,'data.total_failed'),
				fp: _.round(_.multiply(_.get(params,'data.threshold'),100),2),
				per: _.round(_.multiply(_.get(params,'data.threshold'),100),2),
				template_title: consequence.data.meta_data.inapp_title,
		        template_subtitle: consequence.data.meta_data.inapp_subtitle,
		        template_image: consequence.data.meta_data.inapp_right_image,
		        profile_id: _.get(params,'data._id','').toString(),
		        template_collection: consequence.data.meta_data.inapp_template_collection,
		        template_collection_title: consequence.data.meta_data.inapp_template_collection_title,
			}
			let message = {
			    eMail: {
					"templateName": consequence.data.meta_data.templateName,
					"details": {
						"subject": consequence.data.meta_data.subject,
						"Reply-To": "",
						"merge_language": "mailchimp",
						"global_merge_vars": consequence.data.meta_data.global_merge_vars
					}
			    },
			    Activity: {
			      "activityType": "Inbox",
			      "notificationType": "InApp",
			      "template": "seller_failure"
			    }
			}
			let notificaiton_object = {
				type: "profiles",
				directMessage: true,
				receivers: [_.get(params,'data._id','').toString()],
				message,
				data
			}
			_.extend(params,{notificaiton_object : notificaiton_object,meta_data:consequence.data.meta_data});
			_.map(_.get(consequence,'data.do'),(action) => actions[action] = params);
			filter_actions = getValidActions({params,actions});
		});
	}
	else {
		_.map(consequences,(consequence) => {
			let data = {};
			data[_.get(params,'data._id','').toString()] = {
				name: !_.isEmpty(_.get(params,'data.user.name.first')) ? _.get(params,'data.user.name.first') + " " + _.get(params,'data.user.name.last') : _.get(params,'data.user.username'),
				tos: _.get(params,'data.total'),
				toc: _.get(params,'data.total_completed'),
				tof: _.get(params,'data.total_failed'),
				fp: _.round(_.multiply(_.get(params,'data.threshold'),100),2),
				template_image: consequence.data.meta_data.inapp_right_image,
				profile_id: _.get(params,'data._id','').toString(),
				seller_performance: consequence.data.meta_data.seller_performance,
				badge_name: consequence.data.meta_data.badge_name
			}
			let message = {
			    Activity: {
			      "activityType": "Inbox",
			      "notificationType": "InApp",
			      "template": (consequence.data.do[0] === 'removeBadge') ? "remove_profile_badges" : "profile_badges"
			    }
			}
			let notificaiton_object = {
				type: "profiles",
				directMessage: true,
				receivers: [_.get(params,'data._id','').toString()],
				message,
				data
			}
			_.extend(params,{notificaiton_object : notificaiton_object,meta_data:consequence.data.meta_data});
			_.map(_.get(consequence,'data.do'),(action) => actions[action] = params);
			filter_actions = getValidActions({params,actions});
		});
	}
	callback(null,filter_actions);
}

const getValidActions = ({params,actions}) => {
  let processes = notification_processes({params});
  let filter_process = {};
  _.map(processes,(process,key) => {
  	if(actions[key]) filter_process[key] = process;
  });
  console.log('got filter processes ',filter_process);
  return filter_process;
};

const validateRules = (rules,data) => jsonLogic.apply(rules, data);