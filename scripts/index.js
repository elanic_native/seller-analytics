const {argv} = require('yargs');
const stream = require('../streams/stream');
const async = require('async');
const _ = require('lodash');
const actions = require('../loaders/action');
const database_factory = require('../utils/database_factory'); 
let limit = argv.limit || 1000;
let skip = argv.skip || 0;
let isLast = false;

let job = new stream({name : argv.job_name,input_database : argv.read_database,output_database : argv.write_database,entity : argv.write_entity,group : argv.job_group});
async.auto({
	openReadDatabaseConnection: (callback) => {
		let database =  database_factory.getDB({name : `${job.input_database}`});
		let db = new database();
		db.initDB({},(error,database_connection) => {
			callback(error,{db,database_connection});
		});
	},
	openWriteDatabaseConnection: (callback) => {
		let database =  database_factory.getDB({name : `${job.output_database}`});
		let db = new database();
		db.initDB({},(error,database_connection) => {
			callback(error,{db,database_connection});
		});
	},
	runJob : ['openReadDatabaseConnection','openWriteDatabaseConnection',(callback,results) => {
		let read_database = results.openReadDatabaseConnection;
		let write_database = results.openWriteDatabaseConnection;
		async.until(() => isLast === true, (cb) => {
			async.auto({
				execQuery: (callback) => {
					read_database.db.execQueries({job,db : read_database.database_connection,skip,limit,type: 'pipeline',time_duration : argv.days,subtract_date : argv.subtract_date},callback);	
				},
				storeResult: ['execQuery',(callback,result) => {
					let data = _.clone(result.execQuery);
					if(_.isEmpty(data)) return callback();
					write_database.db.writeOutput({job,db : write_database.database_connection,data},callback);
				}],
				checkFlag: ['execQuery',(callback,results) => {
					skip += limit;
					isLast = _.isEqual(_.get(results,'execQuery').length,0);
					callback();
				}]
			},(error) => {
				cb(error);
			});
		},(error) => {
			read_database.database_connection.close();
			write_database.database_connection.close();
			callback(error);
		})
	}]
},(error) => {
	if(error) console.log('Error occured ',error);
})