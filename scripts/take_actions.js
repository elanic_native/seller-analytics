const {argv} = require('yargs');
const stream = require('../streams/stream');
const moment = require('moment');
const async = require('async');
const _ = require('lodash');
const actions = require('../loaders/action');
const database_factory = require('../utils/database_factory'); 
let limit = argv.limit || 1000;
let skip = argv.skip || 0;
let isLast = false;

let job = new stream({name : argv.job_name,input_database : argv.read_database,output_database : argv.write_database,entity : argv.write_entity,group : argv.job_group});

async.auto({
	openReadDatabaseConnection: (callback) => {
		let database =  database_factory.getDB({name : `${job.input_database}`});
		let db = new database();
		db.initDB({},(error,database_connection) => {
			console.log('Read database connected');
			callback(error,{db,database_connection});
		});
	},
	openWriteDatabaseConnection: (callback) => {
		let database =  database_factory.getDB({name : `${job.output_database}`});
		let db = new database();
		db.initDB({},(error,database_connection) => {
			console.log('Write database connected');
			callback(error,{db,database_connection});
		});
	},
	runJob : ['openReadDatabaseConnection','openWriteDatabaseConnection',(callback,results) => {
		let read_database = results.openReadDatabaseConnection;
		let write_database = results.openWriteDatabaseConnection;
		async.until(() => isLast === true, (cb) => {
			async.auto({
				execQuery: (callback) => {
					read_database.db.execQueries({job,db : read_database.database_connection,skip,limit,type: 'output'},callback);	
				},
				getExistingTemplateView: ['execQuery',(callback,results) => {
					let authors = _.map(_.get(results,'execQuery'),'_id');
					let filter = {author : {$in : authors},job_name : job.name};
					let selected_field = 'last_sent_ts';
					write_database.db.execSimpleQueries({job,db : write_database.database_connection,filter,select : selected_field},callback);	
				}],
				takeActions: ['execQuery','getExistingTemplateView',(callback,results) => {
					let data = _.get(results,'execQuery');
					let existing_view = _.get(results,'getExistingTemplateView');
					let last_time_sent_hash = {};
					let user_tag_hash = {};
					_.map(existing_view,(view) => {
						last_time_sent_hash[_.get(view,'author','').toString()] = _.pick(view,['last_sent_ts','last_action']);
						user_tag_hash[_.get(view,'author','').toString()] = _.get(view,'user.tags');
					});
					let records = [];
					let iterator = (record,next) => {
						let total_failed = 0,total_order = 0,total_completed = 0;
						let doc_chunks = _.chunk(_.get(record,'doc'),moment().subtract(1, 'months').daysInMonth());
						_.map(doc_chunks[0],(doc) => {
								total_completed += doc.total_completed;
								total_failed += doc.total_failed;
								total_order += doc.total;
						});
						if(total_order < 5 && doc_chunks.length > 1) {
							_.map(doc_chunks[1],(doc) => {
								if(total_order < 5) {
									total_failed += doc.total_failed;
									total_order += doc.total;
								}
							});
						}
						record.total = total_order;
						record.total_failed = total_failed;
						record.total_completed = total_completed;
						record.threshold = record.total_failed/record.total;
						if(_.get(last_time_sent_hash[record._id.toString()],'')) {
							let date_now = moment(new Date());
							let last_send_date = moment(last_time_sent_hash[record._id.toString()]);
							if(date_now.diff(last_send_date, 'weeks') < 1) {
								records.push(record);
								return next();
							}
						}
						record.last_action = _.get(last_time_sent_hash[record._id.toString()],'last_action');
						record.last_sent_ts = _.get(last_time_sent_hash[record._id.toString()],'last_sent_ts');
						record.last_action_days = moment().startOf('day').diff(moment(record.last_sent_ts).startOf('day'),'days');
						let user_tags = user_tag_hash[record._id.toString()];
						let seller_vacation_index = _.findIndex(user_tags,tag => _.isEqual(_.get(tag,'tag'),'exempt_seller_failure'));
						if(record.total < 5 || (seller_vacation_index > -1 && moment(user_tags[seller_vacation_index].expired_on).isSameOrAfter())) return next();
						actions.loadActions({params : {job,data : record}},(error,action) => {
							record.last_action = (!_.isEmpty(action)) ? _.head(action) : record.last_action;
							record.last_sent_ts = (!_.isEmpty(action)) ? new Date() : record.last_sent_ts;
							record.last_action_days = _.gte(_.get(record,'last_action_days'),15) ? 0 : record.last_action_days;
							records.push(record);
							next();
						});
					}
					async.each(data,iterator,(error) => {
						if(error) {
							console.log('ERROR in taking action ',error);
						}
						console.log('Actions are taken ');
						callback(error,records);
					});
				}],
				storeResult: ['execQuery','takeActions',(callback,result) => {
					let data = _.clone(result.takeActions);
					if(_.isEmpty(data)) return callback();
					write_database.db.updateBulk({job,db : write_database.database_connection,data},callback);
				}],
				checkFlag: ['execQuery',(callback,results) => {
					skip += limit;
					isLast = _.isEqual(_.get(results,'execQuery').length,0);
					callback();
				}]
			},(error) => {
				cb(error);
			});
		},(error) => {
			if(error) console.log('Actions ',error);
			read_database.database_connection.close();
			write_database.database_connection.close();
			console.log('DATABASE connection closed');
			callback(error);
		})
	}]
},(error) => {
	if(error) console.log('Error occured ',error);
})