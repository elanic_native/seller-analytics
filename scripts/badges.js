const {argv} = require('yargs');
const stream = require('../streams/stream');
const moment = require('moment');
const async = require('async');
const _ = require('lodash');
const actions = require('../loaders/action');
const database_factory = require('../utils/database_factory'); 
let limit = argv.limit || 500;
let skip = argv.skip || 0;
let isLast = false;

let job = new stream({name : argv.job_name,input_database : argv.read_database,output_database : argv.write_database,entity : argv.write_entity,group : argv.job_group});

async.auto({
	openReadDatabaseConnection: (callback) => {
		let database =  database_factory.getDB({name : `${job.input_database}`});
		let db = new database();
		db.initDB({},(error,database_connection) => {
			console.log('Read database connected');
			callback(error,{db,database_connection});
		});
	},
	openWriteDatabaseConnection: (callback) => {
		let database =  database_factory.getDB({name : `${job.output_database}`});
		let db = new database();
		db.initDB({},(error,database_connection) => {
			console.log('Write database connected');
			callback(error,{db,database_connection});
		});
	},
	getBadges: ['openReadDatabaseConnection',(callback,results) => {
		let read_database = results.openReadDatabaseConnection;
		let filter = {'value' : argv.badges};
		read_database.db.execSimpleQueries({job,db : read_database.database_connection,filter,entity: 'answers'},callback);	
	}],
	getExistingUsers: ['openReadDatabaseConnection','getBadges',(callback,results) => {
		let read_database = results.openReadDatabaseConnection;
		let badges = results.getBadges;
		let badges_id = _.map(badges,'_id');
		let filter = {'badges' : {$in : badges_id}};
		read_database.db.execSimpleQueries({job,db : read_database.database_connection,filter,entity: argv.read_entity_key},callback);	
	}],
	runJob : ['openReadDatabaseConnection','openWriteDatabaseConnection','getExistingUsers','getBadges',(callback,results) => {
		let read_database = results.openReadDatabaseConnection;
		let write_database = results.openWriteDatabaseConnection;
		let existing_segment_users = results.getExistingUsers;
		let badges = _.map(results.getBadges,'_id');
		let users = _.map(existing_segment_users,user => ({_id : user._id,badges : user.badges}));
		async.until(() => isLast === true, (cb) => {
			async.auto({
				execQuery: (callback) => {
					read_database.db.execQueries({job,db : read_database.database_connection,skip,limit,type: 'output'},callback);	
				},
				takeActions: ['execQuery',(callback,results) => {
					const data = _.map(_.get(results,'execQuery'),record => {
						const existing_badge_index = _.findIndex(users,user => _.isEqual(_.toString(user._id),_.toString(record._id)));
      					const existing_badge = (existing_badge_index > -1) ? _.intersectionWith(users[existing_badge_index].badges,badges,_.toString) : '';
						_.extend(record,{existing_badge : _.toString(_.head(existing_badge))});
						return record;
					});
					actions.runJob[job.name]({params : {job,data,users,badges}},callback);
				}],
				storeResult: ['execQuery','takeActions',(callback,result) => {
					let data = _.clone(result.takeActions);
					if(_.isEmpty(data)) return callback();
					write_database.db.updateBulk({job,db : write_database.database_connection,data},callback);
				}],
				checkFlag: ['execQuery',(callback,results) => {
					skip += limit;
					isLast = _.isEqual(_.get(results,'execQuery').length,0);
					callback();
				}]
			},(error) => {
				cb(error);
			});
		},(error) => {
			if(error) console.log('Actions ',error);
			read_database.database_connection.close();
			write_database.database_connection.close();
			console.log('DATABASE connection closed');
			callback(error);
		})
	}]
},(error) => {
	if(error) console.log('Error occured ',error);
})