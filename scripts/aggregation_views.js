const _ = require('lodash');
const async = require('async');
const $ = require('highland');
const {argv} = require('yargs');
const Query = require('../libraries/Query');
const Cron = require('../libraries/Cron');
const JobRunner = require('../libraries/JobRunner');
const queries = require('../utils/queries');

const runner = new JobRunner();

const runQuery = (_query, cron) => runner.runJob({
  query: new Query({ _query }),
  cron: new Cron(cron)
});

console.log(queries[argv.job_name]);
runQuery(queries[argv.job_name],argv.frequency || queries[argv.job_name].frequency);