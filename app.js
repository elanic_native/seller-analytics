const express = require('express');
const app = express();
const port = 3000;
const mongoose = require('mongoose');
const routes = require('./routes');


// If the Node process ends, close the Mongoose connection


app.use('/', routes);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));