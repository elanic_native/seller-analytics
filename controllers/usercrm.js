const ObjectId = require('mongodb').ObjectID;
const db = require('../utils/database_handlers/db');

exports.usercrm = (request, response, next) => {
    const { author } = request.params;
    db.getCollection('usercrm',async (err, collection) => {
        if (err) {
            return response.status(400).send({
                success: false,
                error: { message: "database connection failed" }
            });
        }
        const filter = { _id: ObjectId(author) };
        const projection = { profile: 1, engagement: 1, posts: 1, purchase: 1, platform: 1, sales: 1, };
        const data = await collection.find(filter, projection).toArray();
        response.send({
            success: true,
            data
        });
    });
};