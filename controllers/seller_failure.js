
const { exec } = require('child_process');

exports.seller_analytics = async (request, response) => {
    const command = 'node scripts/index.js --read_database=mongo --write_database=mongo --write_entity=seller_analytics --job_name=fail_rate --job_group=orders --days=1 --subtract_date=0';
    exec(command, (error, stdout, stderr) => {
        if (error) return response.send({
            success: false,
            error: { message: "error while runing script seller_analytics" }
        });
        response.send({
            success: true,
            data: { message: "Script seller_analytics ran successfully" }
        });
    });
};
exports.seller_analytics_action = async (request, response) => {
    const command = 'node scripts/take_actions.js --read_database=mongo --job_name=fail_rate --job_group=seller_analytics --write_database=mongo --write_entity=seller_materialized_view';
    exec(command, (error, stdout, stderr) => {
        if (error) return response.send({
            success: false,
            error: { message: "error while runing script seller_analytics_action" }
        });
        response.send({
            success: true,
            data: { message: "Script seller_analytics_action ran successfully" }
        });
    });
};