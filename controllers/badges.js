
const ObjectId = require('mongodb').ObjectID;
const db = require('../utils/database_handlers/db');
const { exec } = require('child_process');

exports.feedback_price = async (request, response) => {
  const command = 'node scripts/index.js  --read_database=mongo  --write_database=mongo  --write_entity=seller_analytics  --job_name=feedback_price  --job_group=orders --days=1';
  exec(command, (error, stdout, stderr) => {
    console.info({stdout});
    console.info({stderr});
    if (error) return response.send({
      success: false,
      error: { message: "error while runing script feedback_price" }
    });
    response.send({
      success: true,
      data: { message: "Script feedback_price ran successfully" }
    });
  });
};
exports.feedback_price_action = async (request, response) => {
  const command = 'node scripts/badges.js  --read_database=mongo --read_entity_key=userprofiles --job_name=feedback_price --job_group=seller_analytics --write_database=mongo --write_entity=seller_materialized_view --badges=feedback_price';
  exec(command, (error, stdout, stderr) => {
    if (error) return response.send({
      success: false,
      error: { message: "error while runing script feedback_price_action" }
    });
    response.send({
      success: true,
      data: { message: "Script feedback_price_action ran successfully" }
    });
  });
};
exports.feedback_quality = async (request, response) => {
  const command = 'node scripts/index.js  --read_database=mongo --write_database=mongo --write_entity=seller_analytics --job_name=feedback_quality --job_group=orders --days=1';
  exec(command, (error, stdout, stderr) => {
    if (error) return response.send({
      success: false,
      error: { message: "error while runing script feedback_quality" }
    });
    response.send({
      success: true,
      data: { message: "Script feedback_quality ran successfully" }
    });
  });
};
exports.feedback_quality_action = async (request, response) => {
  const command = 'node scripts/badges.js  --read_database=mongo --read_entity_key=userprofiles --job_name=feedback_quality --job_group=seller_analytics --write_database=mongo --write_entity=seller_materialized_view --badges=feedback_quality';
  exec(command, (error, stdout, stderr) => {
    if (error) return response.send({
      success: false,
      error: { message: "error while runing script feedback_quality_action" }
    });
    response.send({
      success: true,
      data: { message: "Script feedback_quality_action ran successfully" }
    });
  });
};
exports.fill_rate = async (request, response) => {
  const command = 'node scripts/index.js  --read_database=mongo --write_database=mongo --write_entity=seller_analytics --job_name=fill_rate  --job_group=orders --days=1';
  exec(command, (error, stdout, stderr) => {
    if (error) return response.send({
      success: false,
      error: { message: "error while runing script fill_rate" }
    });
    response.send({
      success: true,
      data: { message: "Script fill_rate ran successfully" }
    });
  });
};
exports.fill_rate_action = async (request, response) => {
  const command = 'node scripts/badges.js  --read_database=mongo --read_entity_key=userprofiles --job_name=fill_rate --job_group=seller_analytics --write_database=mongo --write_entity=seller_materialized_view --badges=fill_rate';
  exec(command, (error, stdout, stderr) => {
    if (error) return response.send({
      success: false,
      error: { message: "error while runing script fill_rate_action" }
    });
    response.send({
      success: true,
      data: { message: "Script fill_rate_action ran successfully" }
    });
  });
};
exports.pickup_median_rate = async (request, response) => {
  const command = 'node scripts/index.js  --read_database=mongo --write_database=mongo --write_entity=seller_analytics --job_name=pickup_median_rate --job_group=orders --days=1';
  exec(command, (error, stdout, stderr) => {
    if (error) return response.send({
      success: false,
      error: { message: "error while runing script pickup_median_rate" }
    });
    response.send({
      success: true,
      data: { message: "Script pickup_median_rate ran successfully" }
    });
  });
};
exports.pickup_median_rate_action = async (request, response) => {
  const command = 'node scripts/badges.js  --read_database=mongo --read_entity_key=userprofiles  --job_name=pickup_median_rate --job_group=seller_analytics --write_database=mongo --write_entity=seller_materialized_view --badges=pickup_rate';
  exec(command, (error, stdout, stderr) => {
    if (error) return response.send({
      success: false,
      error: { message: "error while runing script pickup_median_rate_action" }
    });
    response.send({
      success: true,
      data: { message: "Script pickup_median_rate_action ran successfully" }
    });
  });
};

exports.badges = async (request, response, next) => {
  const { job_name, author } = request.params;
  db.getCollection('seller_materialized_view', async (err, collection) => {
    if (err)
      return response.status(400).send({
        success: false,
        data: {},
        error: {
          message: "database connection failed"
        }
      });
    const filter = {
      job_name,
      author: ObjectId(author)
    };
    const projection = {
      total: 1,
      total_failed: 1,
      total_completed: 1,
      threshold: 1,
      last_sent_ts: 1,
      job_name: 1,
    };
    const data = await collection.find(filter, projection).toArray();
    response.send({
      success: true,
      data
    });
  });
};