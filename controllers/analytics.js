const ObjectId = require('mongodb').ObjectID;
const db = require('../utils/database_handlers/db');
const { exec } = require('child_process');
const _ = require('lodash');

exports.profilesAnalytics = async (request, response, next) => {
  const limit = _.toNumber(_.get(request, 'query.limit')) || 24;
  const skip = _.toNumber(_.get(request, 'query.skip')) || 0;

  db.getCollection('user_analytics',async (err, collection) => {
    if (err) {
      response.status(400).send({
        success: false,
        data: {},
        error: {
          message: "database connection failed"
        }
      });
    }
    const data = await collection.find({}).skip(skip).limit(limit).toArray();
    const count = await collection.find({}).count();
    try {
      const resData = data.map(analysis => {
        return {
          total_profile_views: analysis.profile_views_sum,
          profile_views: analysis.profile_views,
          author: analysis.profile_id

        }
      })
      response
        .status(200)
        .send({
          limit,
          skip,
          count,
          success: true,
          data: resData,
          error: {}
        })
    } catch (err) {
      response
        .status(400)
        .send({
          success: false,
          error: {
            message: 'Something Went Wrong!'
          }
        })
    }

  });

};

exports.profileAnalytics = async (request, response, next) => {
  const author = request.params.author;
  const filter = {
    profile_id: ObjectId(author)
  }
  db.getCollection('user_analytics',async (err, collection) => {
    const data = await collection.findOne(filter);
    try {
      const resData = {
        total_profile_views: data.profile_views_sum,
        profile_views: data.profile_views

      }
      response
        .status(200)
        .send({
          success: true,
          data: resData,
        })
    } catch (err) {
      response
        .status(400)
        .send({
          success: false,
          error: {
            message: 'Something Went Wrong!'
          }
        })
    }

  })

};

exports.userPostAnalytics = async (request, response, next) => {
  const author = request.params.author;
  const filter = {
    profile_id: ObjectId(author)
  }
  db.getCollection('user_posts_analytics', async (err, collection) => {
    const data = await collection.findOne(filter);
    try {
      const resData = {
        total_post_views: data.post_views_sum,
        post_views: data.post_views

      };
      response
        .status(200)
        .send({
          success: true,
          data: resData,
        });
    } catch (err) {
      response
        .status(400)
        .send({
          success: false,
          error: {
            message: 'Something Went Wrong!'
          }
        });
    }
  });
};

exports.usersPostAnalytics = async (request, response, next) => {
  const limit = _.toNumber(_.get(request, 'query.limit')) || 24;
  const skip = _.toNumber(_.get(request, 'query.skip')) || 0;

  db.getCollection('user_posts_analytics', async (err, collection) => {
    const data = await collection.find({}).skip(skip).limit(limit).toArray();
    const count = await collection.find({}).count();
    try {
      const resData = data.map(analysis => {
        return {
          total_post_views: analysis.post_views_sum,
          post_views: analysis.post_views,
          author: analysis.profile_id

        }
      })
      response
        .status(200)
        .send({
          limit,
          skip,
          count,
          success: true,
          data: resData
        })
    } catch (err) {

      response
        .status(400)
        .send({
          success: false,
          error: {
            message: 'Something Went Wrong!'
          }
        })
    }

  })


};

exports.user_post_analytics = async (request, response) => {
  const command = 'node scripts/aggregation_views.js --job_name=profile_post_view --frequency=\'0 */3 * * *\'';
  exec(command, (error, stdout, stderr) => {
    if (error) return response.send({
      success: false,
      error: { message: "error while runing script user_post_analytics" }
    });
    response.send({
      success: true,
      data: { message: "Script user_post_analytics ran successfully" }
    });
  });
};

exports.post_analytics = async (request, response) => {
  const command = 'node scripts/aggregation_views.js --job_name=post_view --frequency=\'0 */3 * * *\'';
  exec(command, (error, stdout, stderr) => {
    if (error) return response.send({
      success: false,
      error: { message: "error while runing script post_analytics" }
    });
    response.send({
      success: true,
      data: { message: "Script post_analytics ran successfully" }
    });
  });
};

exports.user_analytics = async (request, response) => {
  const command = 'node scripts/aggregation_views.js --job_name=profile_view --frequency=\'0 */3 * * *\'';
  exec(command, (error, stdout, stderr) => {
    if (error) return response.send({
      success: false,
      error: { message: "error while runing script user_analytics" }
    });
    response.send({
      success: true,
      data: { message: "Script user_analytics ran successfully" }
    });
  });
};